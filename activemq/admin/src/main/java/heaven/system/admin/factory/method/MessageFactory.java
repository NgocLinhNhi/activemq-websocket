package heaven.system.admin.factory.method;

import heaven.system.admin.handler.ManagerRequestHandler;
import heaven.system.admin.handler.ProducerUpdateHandler;
import heaven.system.admin.handler.ProductUpdateHandler;
import heaven.system.admin.message.msgimpl.ListParamAdminInfo;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

public class MessageFactory {
    private static MessageFactory INSTANCE;

    public static MessageFactory getInstance() {
        if (INSTANCE == null) INSTANCE = new MessageFactory();
        return INSTANCE;
    }

    //use design pattern factory to manage handler multiple request
    //gọi đến handler nào thì sẽ xử lý handler đấy
    @Getter
    private final Map<Integer, ManagerRequestHandler> handlers;

    private MessageFactory() {
        this.handlers = new HashMap<>();
        this.handlers.put(ListParamAdminInfo.MSG_TYPE_PRODUCT, new ProductUpdateHandler());
        this.handlers.put(ListParamAdminInfo.MSG_TYPE_PRODUCER, new ProducerUpdateHandler());
    }
}
