package heaven.system.admin.message.entity;

import heaven.system.admin.message.msgimpl.AdminMessageImpl;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ProductMessage extends AdminMessageImpl implements Serializable {
    private String productName;
    private String age;
    private String address;

    public ProductMessage(String target, int commandType) {
        super(target, commandType);
    }
}
