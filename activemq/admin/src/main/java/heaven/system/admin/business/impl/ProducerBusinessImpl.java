package heaven.system.admin.business.impl;

import heaven.system.admin.business.interfaces.IBusiness;
import heaven.system.admin.message.entity.ProducerMessage;
import heaven.system.admin.message.msgimpl.AdminMessageImpl;

public class ProducerBusinessImpl implements IBusiness<ProducerMessage> {

    //Method xử lý nghiệp vụ cho step doHandle() của producer
    //Được giấu sâu dưới design pattern - factory method
    @Override
    public int insert(ProducerMessage request) {
        try {
            System.out.println("OK THIS IS  STEP HANDLER FOR PRODUCER ");
        } catch (IllegalArgumentException e) {
            return AdminMessageImpl.PROCESS_FAIL_STATUS;
        }
        return AdminMessageImpl.PROCESS_SUCCESSFUL_STATUS;
    }

    @Override
    public int update(ProducerMessage request) {
        return 0;
    }

    @Override
    public int findById(ProducerMessage request) {
        return 0;
    }

}
