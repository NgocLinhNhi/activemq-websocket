package heaven.system.admin.factory.method;

import heaven.system.admin.business.impl.ProducerBusinessImpl;
import heaven.system.admin.business.impl.ProductIBusinessImpl;
import heaven.system.admin.business.interfaces.IBusiness;
import heaven.system.admin.enums.BusinessTypeEnum;

import java.util.HashMap;
import java.util.Map;

public class BusinessFactory {

    private static BusinessFactory INSTANCE;

    public static BusinessFactory getInstance() {
        if (INSTANCE == null) INSTANCE = new BusinessFactory();
        return INSTANCE;
    }

    // Cách 2 : xử lý Factory by Enum
    public IBusiness getFactory(BusinessTypeEnum type) {
        switch (type) {
            case PRODUCT:
                return new ProductIBusinessImpl();
            case PRODUCER:
                return new ProducerBusinessImpl();
            default:
                throw new UnsupportedOperationException("This furniture is unsupported ");
        }
    }

    // Cách 2 : xử lý Factory by HashMap
    private final Map<BusinessTypeEnum, IBusiness> handlers;

    private BusinessFactory() {
        this.handlers = new HashMap<>();
        this.handlers.put(BusinessTypeEnum.PRODUCT, new ProductIBusinessImpl());
        this.handlers.put(BusinessTypeEnum.PRODUCER, new ProducerBusinessImpl());
    }

    public IBusiness getBusiness(BusinessTypeEnum businessType) {
        return handlers.get(businessType);
    }
}
