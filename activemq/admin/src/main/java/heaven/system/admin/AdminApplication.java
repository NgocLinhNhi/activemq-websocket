package heaven.system.admin;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import static org.springframework.boot.SpringApplication.run;

@SpringBootApplication
public class AdminApplication {

    public static void main(String[] args) {
        run(AdminApplication.class, args);
    }

}
