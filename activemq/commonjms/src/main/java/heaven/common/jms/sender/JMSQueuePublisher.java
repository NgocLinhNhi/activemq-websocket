package heaven.common.jms.sender;

import heaven.common.jms.JMSQueue;

import java.util.Properties;


/**
 *
 * Xác định kiểu JMS là Queue và là Publisher
 *
 */
public class JMSQueuePublisher extends JMSQueue {

    public JMSQueuePublisher(Properties properties) {
        super(properties);
        this.queueMode = QUE_MODE_SENDER;
    }

}
