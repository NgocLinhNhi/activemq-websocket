package heaven.common.jms.concurrent;

import heaven.common.jms.Interface.NFuture;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class NFutureConcurrentHashMap<K> extends NFutureAbstractMap<K> {

    @Override
    protected Map<K, NFuture> newFutureMap() {
        return new ConcurrentHashMap<>();
    }

}