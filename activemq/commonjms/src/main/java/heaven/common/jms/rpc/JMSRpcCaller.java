package heaven.common.jms.rpc;

import heaven.common.jms.Interface.NFuture;
import heaven.common.jms.concurrent.NFutureConcurrentHashMap;
import heaven.common.jms.Interface.NFutureMap;
import heaven.common.jms.exception.MaxCapacityException;
import heaven.common.jms.proto.RpcAms;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.*;
import java.io.Serializable;
import java.util.UUID;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicLong;

public class JMSRpcCaller {

    @Setter
    protected String name;
    @Setter
    protected Session session;
    @Setter
    protected int maxCapacity;
    @Setter
    protected MessageProducer requestProducer;
    @Setter
    protected MessageConsumer responseConsumer;

    protected volatile boolean active;

    protected final String endpointId;
    protected final AtomicLong requestCount;
    protected final NFutureMap<String> futures;

    public static final int DEFAULT_TIMEOUT = 10000;

    Logger logger = LoggerFactory.getLogger(JMSRpcCaller.class);

    public JMSRpcCaller() {
        this.requestCount = new AtomicLong();
        this.endpointId = UUID.randomUUID().toString();
        this.futures = new NFutureConcurrentHashMap<>();
    }

    public void start() throws Exception {
        if (responseConsumer != null) {
            Thread newThread = new Thread(this::loop);
            newThread.setName("rpc-caller-" + name);
            newThread.start();
        }
    }

    public void stop() {
        this.active = false;
    }

    public void send(RpcAms.RpcMessage request) throws Exception {
        send(generateRequestId(), request);
    }

    protected void send(String id, RpcAms.RpcMessage request) throws Exception {
        BytesMessage message = session.createBytesMessage();
        message.setJMSCorrelationID(id);
        message.writeBytes(request.toByteArray());
        this.requestProducer.send(message);
    }

    public void send(Serializable request) throws JMSException {
        send(generateRequestId(), request);
    }

    protected void send(String id, Serializable request) throws JMSException {
        ObjectMessage message = session.createObjectMessage();
        message.setJMSCorrelationID(id);
        message.setObject(request);
        this.requestProducer.send(message);
    }

    public <T> T call(Object request) throws Exception {
        return call(request, DEFAULT_TIMEOUT);
    }

    public <T> T call(Object request, int timeout) throws Exception {
        String requestId = generateRequestId();
        int futureCount = this.futures.size();
        if (futureCount >= maxCapacity)
            throw new MaxCapacityException("max capacity: " + maxCapacity);
        NFuture future = this.futures.addFuture(requestId);
        try {
            if (request instanceof Serializable)
                send(requestId, (Serializable) request);
            else if (request instanceof RpcAms.RpcMessage) // xử lý với file proto
                send(requestId, (RpcAms.RpcMessage) request);
            else
                future.setException(new IllegalArgumentException("unsupport request of type: " + request.getClass().getTypeName()));
        } catch (TimeoutException e) {
            this.futures.removeFuture(requestId);
            throw e;
        }
        T answer = future.get(timeout);
        return answer;
    }

    protected String generateRequestId() {
        String requestId = endpointId + ":" + requestCount.incrementAndGet();
        return requestId;
    }

    protected void loop() {
        this.active = true;
        while (active) {
            handleReceivedMessages();
        }
    }

    protected void handleReceivedMessages() {
        NFuture future = null;
        String requestId = null;
        Object responseData = null;
        Exception exception = null;
        try {
            Message message = responseConsumer.receive();
            requestId = message.getJMSCorrelationID();
            future = futures.removeFuture(requestId);
            responseData = getResponseData(message);
        } catch (Exception e) {
            logger.info(name + "::handle received message error", e);
            exception = e;
        }
        if (future == null) {
            logger.info("has no future map to request id: {}, it may timeout", requestId);
        } else {
            if (responseData != null)
                future.setResult(responseData);
            else
                future.setException(exception);
        }
    }

    protected Object getResponseData(Message message) throws Exception {
        return JMSRpcMessageDeserializers.deserialize(message);
    }

}
