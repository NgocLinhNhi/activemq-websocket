package heaven.common.jms;

import javax.jms.*;
import java.util.Properties;

/**
 *
 * Xử lý tạo Topic connection , loại subscriber hay publisher và đóng mở
 *
 */
public abstract class JMSTopic extends JMSEndpoint {

	protected Topic topic;
	protected byte topicMode;
	protected TopicSession session;
	protected TopicConnection connection;
	protected TopicPublisher publisher;
	protected TopicSubscriber subscriber;


	protected volatile boolean topicOpened = true;

	public final static byte TOPIC_MODE_PUBLISHER = 0;
	public final static byte TOPIC_MODE_SUBSCRIBER = 1;

	public JMSTopic(Properties properties) {
		super(properties);
	}


	@Override
	public void connectToJMSServer() throws Exception {
        connection = createTopicConnection();
        session = connection.createTopicSession(false,TopicSession.AUTO_ACKNOWLEDGE);
        topic = session.createTopic(name);

        if(topicMode == TOPIC_MODE_PUBLISHER){
            publisher = session.createPublisher(topic);
			publisher.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
        }
        else {
            subscriber = session.createSubscriber(topic);      	
        }

        createMoreComponents();

        connection.start();
	}

	protected void createMoreComponents() throws Exception {}

	public void closeJMSTopic() throws JMSException{
		topicOpened = false;
		
        if(publisher != null){
        	publisher.close(); 
        	publisher = null;
        }
        if(subscriber != null){
        	subscriber.close(); 
        	subscriber = null;
        }
        if(session != null){
        	session.close();
        	session = null;
        }
        if(connection != null){
        	connection.close();		
        	connection = null;
        }
        topic = null;
	}
}
