package heaven.common.jms.concurrent;

import heaven.common.jms.Interface.NFuture;
import heaven.common.jms.Interface.NFutureMap;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class NFutureAbstractMap<K> implements NFutureMap<K> {

    protected final Map<K, NFuture> map;

    public NFutureAbstractMap() {
        this.map = newFutureMap();
    }

    protected abstract Map<K, NFuture> newFutureMap();

    @Override
    public NFuture addFuture(K key) {
        return addFuture(key, new NFutureTask());
    }

    @Override
    public NFuture addFuture(K key, NFuture future) {
        NFuture old = map.putIfAbsent(key, future);
        return old == null ? future : old;
    }

    @Override
    public NFuture putFuture(K key) {
        AtomicBoolean existed = new AtomicBoolean(true);
        NFuture future = map.computeIfAbsent(key, k -> {
            existed.set(false);
            return new NFutureTask();
        });
        if(existed.get())
            throw new NFutureExistedException(key, future);
        return future;
    }

    @Override
    public NFuture getFuture(K key) {
        NFuture future = map.get(key);
        return future;
    }

    @Override
    public NFuture removeFuture(K key) {
        NFuture future = map.remove(key);
        return future;
    }

    @Override
    public int size() {
        int size = map.size();
        return size;
    }

    @Override
    public Map<K, NFuture> clear() {
        Map<K, NFuture> answer = new HashMap<>(map);
        map.clear();
        return answer;
    }

}