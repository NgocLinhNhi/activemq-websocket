package heaven.common.jms.utils;

public interface NSupplier<T> {

    T get() throws Exception;

}
