package heaven.common.jms.constant;



public enum ProtoPayloadClassEnum {
	UPDATE_MAIL_TEMPLATE_REQUEST("UpdateMailTemplateRequest"),
	UPDATE_MAIL_TEMPLATE_RESPONSE("UpdateMailTemplateResponse");
	private String key;
	
	private ProtoPayloadClassEnum(String key){
		this.key = key;
	}

	public String getKey() {
		return key;
	}

}
