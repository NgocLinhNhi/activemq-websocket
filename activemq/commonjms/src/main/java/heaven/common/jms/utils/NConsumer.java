package heaven.common.jms.utils;

public interface NConsumer<T> {

    void accept(T t) throws Exception;

}
