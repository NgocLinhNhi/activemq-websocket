package heaven.common.jms.rpc;

import heaven.common.jms.exception.JMSEndpointClosedException;
import heaven.common.jms.exception.JMSUnsupportMessageTypeException;
import heaven.common.jms.proto.RpcAms;
import heaven.common.jms.utils.NFunction;
import lombok.Setter;

import javax.jms.*;
import java.io.Serializable;

/**
 *
 * Message là file proto 
 *
 */
public class JMSRpcListener {

    @Setter
    protected String name;
    @Setter
    protected Session session;
    @Setter
    protected MessageProducer messageProducer;
    @Setter
    protected MessageConsumer messageConsumer;
    @Setter
    protected NFunction<Object, Object> messageHandler;

    public void listen() throws Exception {
        Message request = messageConsumer.receive();
        if (request == null)
            throw new JMSEndpointClosedException(name + " has closed");
        Object requestData = getRequestData(request);
        try {
            Object result = messageHandler.apply(requestData);
            if (result != null) {
                String requestId = request.getJMSCorrelationID();
                Message response = createResponse(result);
                response.setJMSCorrelationID(requestId);
                messageProducer.send(response);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    protected Message createResponse(Object result) throws JMSException {
        if (result instanceof Serializable) {
            return session.createObjectMessage((Serializable) result);
        } else if (result instanceof MapMessage) {
            return (MapMessage) result;
        } else if (result instanceof RpcAms.RpcMessage) {
            RpcAms.RpcMessage r = (RpcAms.RpcMessage) result;
            BytesMessage message = session.createBytesMessage();
            message.writeBytes(r.toByteArray());
            return message;
        }
        throw new JMSUnsupportMessageTypeException(result.getClass());
    }

    protected Object getRequestData(Message message) throws Exception {
        return JMSRpcMessageDeserializers.deserialize(message);
    }

}
