package heaven.common.jms.sender;

import heaven.common.jms.proto.RpcAms;
import heaven.common.jms.rpc.JMSRpcCaller;

import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import java.io.Serializable;
import java.util.Properties;

public class JMSTopicRpcPublishser extends JMSTopicPublisher {

    protected JMSRpcCaller rpcCaller;
    protected final String responseTo;
    protected MessageConsumer responseConsumer;

    public JMSTopicRpcPublishser(Properties properties) {
        super(properties);
        this.responseTo = properties.getProperty("responseTo");
    }

    @Override
    protected void createMoreComponents() throws Exception {
        this.responseConsumer = createResponseConsumer();
        this.rpcCaller = createRpcCaller();
        this.rpcCaller.start();
    }

    protected MessageConsumer createResponseConsumer() throws Exception {
        MessageConsumer consumer = null;
        if (responseTo != null)
            consumer = session.createConsumer(session.createTopic(responseTo));
        return consumer;
    }

    protected JMSRpcCaller createRpcCaller() {
        JMSRpcCaller caller = new JMSRpcCaller();
        caller.setName(name);
        caller.setSession(session);
        caller.setRequestProducer(publisher);
        caller.setResponseConsumer(responseConsumer);
        return caller;
    }

    public void send(RpcAms.RpcMessage request) throws Exception {
        this.rpcCaller.send(request);
    }

    public void send(Serializable request) throws Exception {
        this.rpcCaller.send(request);
    }

    public <T> T call(Object request) throws Exception {
        return this.rpcCaller.call(request);
    }

    public <T> T call(Object request, int timeout) throws Exception {
        return this.rpcCaller.call(request, timeout);
    }

    @Override
    public void closeJMSTopic() throws JMSException {
        super.closeJMSTopic();
        if (rpcCaller != null)
            rpcCaller.stop();
        this.rpcCaller = null;
        if (responseConsumer != null)
            responseConsumer.close();
        this.responseConsumer = null;
    }
}