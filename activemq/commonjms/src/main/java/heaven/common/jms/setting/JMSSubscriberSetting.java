package heaven.common.jms.setting;

import lombok.Getter;

@Getter
public class JMSSubscriberSetting extends JMSEndpointSetting {

    public JMSSubscriberSetting() {
        this.threadPoolSize = 1;
    }

}
