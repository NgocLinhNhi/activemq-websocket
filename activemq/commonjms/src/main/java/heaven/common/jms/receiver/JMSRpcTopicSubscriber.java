package heaven.common.jms.receiver;

import heaven.common.jms.rpc.JMSRpcListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.JMSException;
import java.util.Properties;

/**
 *
 * Xử lý topic with Rpc - subscriber
 *
 */
public abstract class JMSRpcTopicSubscriber extends JMSTopicSubscriber {

    protected volatile boolean active;
    protected JMSRpcListener rpcListener;

    Logger logger = LoggerFactory.getLogger(JMSRpcTopicSubscriber.class);

    public JMSRpcTopicSubscriber(Properties properties) {
        super(properties);
    }

    public void start() {
        if (active)
            throw new IllegalStateException(name + " has started");
        active = true;
        rpcListener = newRpcListener();
        Thread newThread = new Thread(this, "rpc-subscriber-" + id);
        newThread.start();
    }

    protected JMSRpcListener newRpcListener() {
        JMSRpcListener listener = new JMSRpcListener();
        listener.setName(name);
        listener.setSession(session);
        listener.setMessageProducer(publisher);
        listener.setMessageConsumer(subscriber);
        listener.setMessageHandler(this::handleReceivedMessage);
        return listener;
    }

    @Override
    protected void subscribeMsg() throws JMSException {
        try {
            rpcListener.listen();
        } catch (JMSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("subscribe message error", e);
        }
    }

    protected Object handleReceivedMessage(Object message) {
        onMessage(message);
        return null;
    }

    protected void onMessage(Object message) {
    }

}