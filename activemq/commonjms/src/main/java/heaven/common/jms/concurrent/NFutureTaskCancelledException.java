package heaven.common.jms.concurrent;

public class NFutureTaskCancelledException extends RuntimeException {
    private static final long serialVersionUID = 7012377646935511890L;

    public NFutureTaskCancelledException(String message) {
        super(message);
    }

}