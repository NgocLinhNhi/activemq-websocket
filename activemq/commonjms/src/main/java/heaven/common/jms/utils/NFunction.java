package heaven.common.jms.utils;

public interface NFunction<T, R> {

    R apply(T t) throws Exception;

}
