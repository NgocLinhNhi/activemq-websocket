package heaven.common.jms.exception;

public class MaxCapacityException extends RuntimeException {

    public MaxCapacityException(String msg) {
        super(msg);
    }

}
