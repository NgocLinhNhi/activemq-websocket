package heaven.common.jms.receiver;

import heaven.common.jms.rpc.JMSRpcListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.JMSException;
import java.util.Properties;

/**
 *
 * Xử lý Queue with Rpc - subscriber
 *
 */
public abstract class JMSRpcQueueSubscriber extends JMSQueueSubscriber {

    protected volatile boolean active;
    protected JMSRpcListener rpcListener;

    Logger logger = LoggerFactory.getLogger(JMSRpcQueueSubscriber.class);

    public JMSRpcQueueSubscriber(Properties properties) {
        super(properties);
    }

    public void start() {
        if (active)
            throw new IllegalStateException(name + " has started");
        active = true;
        rpcListener = newRpcListener();
        Thread newThread = new Thread(this, "rpc-receiver-" + id);
        newThread.start();
    }

    protected JMSRpcListener newRpcListener() {
        JMSRpcListener listener = new JMSRpcListener();
        listener.setName(name);
        listener.setSession(session);
        listener.setMessageProducer(sender);
        listener.setMessageConsumer(receiver);
        listener.setMessageHandler(this::handleReceivedMessage);
        return listener;
    }

    @Override
    protected void receiveMsg() throws JMSException {
        try {
            rpcListener.listen();
        } catch (JMSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("receive message error", e);
        }
    }

    protected Object handleReceivedMessage(Object message) {
        onMessage(message);
        return null;
    }

    protected void onMessage(Object message) {
    }

}