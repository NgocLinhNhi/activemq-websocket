package heaven.common.jms;

import heaven.common.jms.utils.InputStreams;
import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;
import javax.naming.InitialContext;
import java.lang.IllegalStateException;
import java.util.Properties;

public final class JMSConnectionFactoryProxy {

    private final String username;
    private final String password;
    private final Properties properties;
    private final InitialContext context;
    private static JMSConnectionFactoryProxy instance;

    private JMSConnectionFactoryProxy() {
        try {
            this.properties = loadProperties();
            this.context = loadContext();
            this.username = properties.getProperty("activemq.username");
            this.password = properties.getProperty("activemq.password");
        } catch (Exception e) {
            throw new IllegalStateException("can't load jms connection factory", e);
        }
    }

    public static JMSConnectionFactoryProxy getInstance() {
        if (instance == null) {
            synchronized (JMSConnectionFactoryProxy.class) {
                if (instance == null)
                    instance = new JMSConnectionFactoryProxy();
            }
        }
        return instance;
    }

    private InitialContext loadContext() throws Exception {
        InitialContext context = new InitialContext(properties);
        return context;
    }

    private Properties loadProperties() throws Exception {
        Properties properties = new Properties();
        properties.load(InputStreams.getInputStream(getPropertiesFile()));
        properties.put("java.naming.provider.url", properties.get("activemq.url"));
        properties.put("java.naming.factory.initial", "org.apache.activemq.jndi.ActiveMQInitialContextFactory");
        properties.put("connectionFactoryNames", "ConnectionFactory,QueueConnectionFactory,TopicConnectionFactory");
        return properties;
    }

    private String getPropertiesFile() {
        String file = System.getProperty("jms.properties.file");
        if (file == null)
            file = "application.properties";
        return file;
    }

    /**
     * Create connection for Database
     *
     * @return
     */
    public Connection createConnection() {
        try {
            ConnectionFactory factory = (ConnectionFactory) context.lookup("ConnectionFactory");
            Connection connection = factory.createConnection(username, password);
            return connection;
        } catch (Exception e) {
            throw new IllegalStateException("connection factory not found", e);
        }
    }

    /**
     * Create connection for Queue
     *
     * @return
     */
    public QueueConnection createQueueConnection() {
        try {
            //QueueConnectionFactory factory = (QueueConnectionFactory) context.lookup("QueueConnectionFactory");
            ActiveMQConnectionFactory factory = (ActiveMQConnectionFactory) context.lookup("QueueConnectionFactory");
            factory.setTrustAllPackages(true);
            QueueConnection queueConnection = factory.createQueueConnection(username, password);
            return queueConnection;
        } catch (Exception e) {
            throw new IllegalStateException("queue connection factory not found", e);
        }
    }

    /**
     * Create connection for Topic
     *
     * @return
     */
    public TopicConnection createTopicConnection() {
        try {
            //cách config cũ của Dũng Khùng không send được objectMessage nếu không config thêm seriablizable
            //TopicConnectionFactory factory = (TopicConnectionFactory) context.lookup("TopicConnectionFactory");
            ActiveMQConnectionFactory factory = (ActiveMQConnectionFactory) context.lookup("TopicConnectionFactory");
            //Tạo kết nối Queue/Topic = ActiveMQConnectionFactory và setTrustAllPackages = true để send được objectMessage dễ dàng
            factory.setTrustAllPackages(true);
            TopicConnection topicConnection = factory.createTopicConnection(username, password);
            return topicConnection;
        } catch (Exception e) {
            throw new IllegalStateException("topic connection factory not found", e);
        }
    }
}
