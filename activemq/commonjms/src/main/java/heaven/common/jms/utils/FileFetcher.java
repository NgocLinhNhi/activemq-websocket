package heaven.common.jms.utils;

import java.io.File;

public interface FileFetcher {

    File getFile(String filePath);

}
