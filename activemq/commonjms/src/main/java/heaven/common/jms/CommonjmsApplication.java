package heaven.common.jms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CommonjmsApplication {

    public static void main(String[] args) {
        SpringApplication.run(CommonjmsApplication.class, args);
    }

}
