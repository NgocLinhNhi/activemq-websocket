package heaven.common.jms.concurrent;

import heaven.common.jms.Interface.NFuture;

public class NFutureExistedException extends IllegalArgumentException {
    private static final long serialVersionUID = 4258315197030448654L;

    public NFutureExistedException(Object key, NFuture old) {
        super("future with key: " + key + " existed: " + old);
    }

}
