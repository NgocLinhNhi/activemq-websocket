package heaven.common.jms.sender;

import heaven.common.jms.JMSTopic;

import java.util.Properties;


/**
 *
 * Xác định kiểu JMS là Topic và là Publisher
 *
 */
public class JMSTopicPublisher extends JMSTopic {

    public JMSTopicPublisher(Properties properties) {
        super(properties);
        this.topicMode = TOPIC_MODE_PUBLISHER;
    }
}
