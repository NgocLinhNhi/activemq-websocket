package heaven.common.jms.exception;

public class JMSEndpointClosedException extends IllegalStateException {

    public JMSEndpointClosedException(String message) {
        super(message);
    }

}
