package heaven.common.jms.setting;

import heaven.common.jms.JMSEndpoint;
import lombok.Getter;

import java.util.Properties;
import java.util.function.Function;

@Getter
public class JMSEndpointSetting {

    protected String id;
    protected String name;
    protected int threadPoolSize;
    protected Function<Properties, JMSEndpoint> supplier;

    public JMSEndpointSetting id(String id) {
        this.id = id;
        return this;
    }

    public JMSEndpointSetting name(String name) {
        this.name = name;
        return this;
    }

    public JMSEndpointSetting threadPoolSize(int threadPoolSize) {
        this.threadPoolSize = threadPoolSize;
        return this;
    }

    public JMSEndpointSetting supplier(Function<Properties, JMSEndpoint> supplier) {
        this.supplier = supplier;
        return this;
    }

    public Properties toProperties() {
        Properties properties = new Properties();
        properties.setProperty("id", id);
        properties.setProperty("name", name);
        return properties;
    }

}
