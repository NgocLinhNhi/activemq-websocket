package heaven.common.jms.sender;

import heaven.common.jms.constant.JMSConstants;
import heaven.common.jms.proto.RpcAms;
import heaven.common.jms.rpc.JMSRpcCaller;

import javax.jms.*;
import java.io.Serializable;
import java.util.Properties;

public class JMSRpcQueuePublisher extends JMSQueuePublisher {

    protected JMSRpcCaller rpcCaller;
    protected final int maxCapacity;
    protected final int responseType;
    protected final String responseTo;
    protected MessageConsumer responseConsumer;

    public JMSRpcQueuePublisher(Properties properties) {
        super(properties);
        this.responseTo = properties.getProperty(JMSConstants.RESPONSE_TO_KEY);
        this.responseType = (int) properties.get(JMSConstants.RESPONSE_TYPE_KEY);
        this.maxCapacity = (int) properties.get(JMSConstants.MAX_CAPACITY_KEY);
    }

    @Override
    protected void createMoreComponents() throws Exception {
        this.responseConsumer = createResponseConsumer();
        this.rpcCaller = createRpcCaller();
        this.rpcCaller.start();
    }

    protected MessageConsumer createResponseConsumer() throws Exception {
        if (responseTo == null)
            return null;
        if (responseType == JMSConstants.RESPONSE_TYPE_QUEUE)
            return session.createConsumer(session.createQueue(responseTo));
        TopicConnection topicConnection = createTopicConnection();
        TopicSession topicSession = topicConnection.createTopicSession(false, TopicSession.AUTO_ACKNOWLEDGE);
        TopicSubscriber answer = topicSession.createSubscriber(topicSession.createTopic(responseTo));
        topicConnection.start();
        return answer;
    }

    protected JMSRpcCaller createRpcCaller() {
        JMSRpcCaller caller = new JMSRpcCaller();
        caller.setName(name);
        caller.setSession(session);
        caller.setMaxCapacity(maxCapacity);
        caller.setRequestProducer(sender);
        caller.setResponseConsumer(responseConsumer);
        return caller;
    }

    public void send(RpcAms.RpcMessage request) throws Exception {
        this.rpcCaller.send(request);
    }

    public void send(Serializable request) throws Exception {
        this.rpcCaller.send(request);
    }

    public <T> T call(Object request) throws Exception {
        return this.rpcCaller.call(request);
    }

    public <T> T call(Object request, int timeout) throws Exception {
        return this.rpcCaller.call(request, timeout);
    }

    @Override
    public void closeJMSQueue() throws JMSException {
        super.closeJMSQueue();
        if (rpcCaller != null)
            rpcCaller.stop();
        this.rpcCaller = null;
        if (responseConsumer != null)
            responseConsumer.close();
        this.responseConsumer = null;
    }
}