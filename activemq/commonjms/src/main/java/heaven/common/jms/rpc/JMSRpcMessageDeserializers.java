package heaven.common.jms.rpc;


import heaven.common.jms.exception.JMSInvalidMessageException;
import heaven.common.jms.exception.JMSUnsupportMessageTypeException;
import heaven.common.jms.proto.RpcAms;

import javax.jms.BytesMessage;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.ObjectMessage;

/**
 *
 * Deserializers message là file proto và dạng byte
 *
 */
public final class JMSRpcMessageDeserializers {

    private JMSRpcMessageDeserializers() {
    }

    public static Object deserialize(Message message) throws Exception {
        if (message instanceof ObjectMessage) {
            return ((ObjectMessage) message).getObject();
        } else if (message instanceof MapMessage) {
            return message;
        } else if (message instanceof BytesMessage) {
            BytesMessage bytesMessage = (BytesMessage) message;
            byte[] data = new byte[(int) bytesMessage.getBodyLength()];
            bytesMessage.readBytes(data);
            RpcAms.RpcMessage rpcResponse = null;
            try {
                rpcResponse = RpcAms.RpcMessage.parseFrom(data);
            } catch (Exception e) {
                throw new JMSInvalidMessageException("invalid message: " + bytesMessage, e);
            }
            return rpcResponse;
        }
        throw new JMSUnsupportMessageTypeException(message.getClass());
    }

}
