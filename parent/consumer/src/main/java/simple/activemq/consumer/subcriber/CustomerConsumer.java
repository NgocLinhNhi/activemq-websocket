package simple.activemq.consumer.subcriber;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import simple.activemq.consumer.interfaces.IReceiveMessage;
import simple.activemq.producer.constant.JMSConstant;
import simple.activemq.producer.entity.CustomerMessage;
import simple.activemq.producer.setting.JMSProducerConfiguration;

import javax.jms.*;


public class CustomerConsumer implements IReceiveMessage {

    private static final Logger logger = LoggerFactory.getLogger(CustomerConsumer.class);

    private static CustomerConsumer INSTANCE;
    private MessageConsumer consumer;

    public static CustomerConsumer getInstance() throws Exception {
        if (INSTANCE == null) INSTANCE = new CustomerConsumer();
        return INSTANCE;
    }

    private CustomerConsumer() throws Exception {
        JMSProducerConfiguration jmsProducerConfiguration = JMSProducerConfiguration.getInstance();
        consumer = createSubscriberMessageType(jmsProducerConfiguration);
    }

    public void subscriberMessage() throws JMSException {
        while (true) {
            receiveObjectMessage();
        }
    }

    @Override
    public void receiveObjectMessage() throws JMSException {
        //Object message
        Message message = consumer.receive();
        if (message instanceof ObjectMessage) {
            ObjectMessage objectMessage = (ObjectMessage) message;
            CustomerMessage customerMessage = (CustomerMessage) objectMessage.getObject();

            logger.info("Message Subscriber CustomerName : {} ==== CustomerAddress : {} ",
                    customerMessage.getCustomerName(),
                    customerMessage.getAddress());
        }
    }

    @Override
    public void receiveTextMessage() throws JMSException {
        //Text message
        Message message = consumer.receive();
        if (message instanceof TextMessage) {
            TextMessage textMessage = (TextMessage) message;
            System.out.println(" Received message " + textMessage.getText() + " ");
        }
    }

    private MessageConsumer createSubscriberMessageType(JMSProducerConfiguration instance) throws Exception {
        Destination destination = instance.createMessageType(JMSConstant.GET_CUSTOMER_QUEUE);
        return instance.session.createConsumer(destination);
    }
}
