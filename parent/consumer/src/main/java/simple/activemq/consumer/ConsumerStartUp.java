package simple.activemq.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import simple.activemq.consumer.subcriber.CustomerConsumer;

public class ConsumerStartUp {
    private static final Logger logger = LoggerFactory.getLogger(ConsumerStartUp.class);

    public static void main(String[] args) {
        try {
            CustomerConsumer.getInstance().subscriberMessage();
        } catch (Exception e) {
            logger.error("Exception {1}", e);
        }
    }
}
