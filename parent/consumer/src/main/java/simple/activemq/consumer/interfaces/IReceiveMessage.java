package simple.activemq.consumer.interfaces;

import javax.jms.JMSException;

public interface IReceiveMessage {
    void receiveObjectMessage() throws JMSException;

    void receiveTextMessage() throws JMSException;
}
