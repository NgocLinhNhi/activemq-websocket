package simple.activemq.producer.interfaces;

public interface IMessage {
    void sendObjectMessage() throws Exception;

    void sendTextMessage() throws Exception;
}
