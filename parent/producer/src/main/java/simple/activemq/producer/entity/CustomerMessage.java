package simple.activemq.producer.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class CustomerMessage implements Serializable {

    private String customerName;
    private int age;
    private String address;

    public CustomerMessage(String customerName, int age, String address) {
        this.customerName = customerName;
        this.age = age;
        this.address = address;
    }
}
