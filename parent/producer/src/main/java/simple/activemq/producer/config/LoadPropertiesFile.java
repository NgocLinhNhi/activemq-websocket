package simple.activemq.producer.config;

import lombok.Getter;
import lombok.Setter;
import simple.activemq.producer.io.InputStreams;

import java.util.Properties;

@Getter
@Setter
public class LoadPropertiesFile {

    private static LoadPropertiesFile INSTANCE;

    public static LoadPropertiesFile getInstance() {
        if (INSTANCE == null) INSTANCE = new LoadPropertiesFile();
        return INSTANCE;
    }

    public Properties loadProperties() throws Exception {
        Properties properties = new Properties();
        properties.load(InputStreams.getInputStream(getPropertiesFile()));
        return properties;
    }

    private String getPropertiesFile() {
        String file = System.getProperty("application.properties");
        if (file == null)
            file = "application.properties";
        return file;
    }

}
