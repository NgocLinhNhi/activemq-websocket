package simple.activemq.producer.producer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import simple.activemq.producer.entity.CustomerMessage;
import simple.activemq.producer.interfaces.IMessage;
import simple.activemq.producer.setting.JMSProducerConfiguration;

import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;

import static simple.activemq.producer.constant.JMSConstant.GET_CUSTOMER_QUEUE;

public class CustomerMessagePublisher implements IMessage {
    private static final Logger logger = LoggerFactory.getLogger(CustomerMessagePublisher.class);

    private static CustomerMessagePublisher INSTANCE;
    private MessageProducer producer;
    private JMSProducerConfiguration instance;

    public static CustomerMessagePublisher getInstance() throws Exception {
        if (INSTANCE == null) INSTANCE = new CustomerMessagePublisher();
        return INSTANCE;
    }

    private CustomerMessagePublisher() throws Exception {
        instance = JMSProducerConfiguration.getInstance();
        producer = createPublisherMessageType();
    }

    public void createMessage() throws Exception {
        // Destination represents here our queue ‘GET_CUSTOMER_QUEUE’ on the JMS server. it will be created automatically.
        while (true) {
            sendObjectMessage();
            //Cứ 10s send message 1 lần
            Thread.sleep(10000);
        }
    }

    @Override
    public void sendObjectMessage() throws Exception {
        //Send Object Message
        ObjectMessage message = instance.session.createObjectMessage(createCustomerMessage());
        logger.info("Message has been Sent !!!!!");
        producer.send(message);
    }

    @Override
    public void sendTextMessage() throws Exception {
        //send textMesage
        String msg = "Hello world ";
        TextMessage message = instance.session.createTextMessage(msg);
        producer.send(message);
        logger.info("Message has been Sent !!!!!");
    }

    private MessageProducer createPublisherMessageType() throws Exception {
        Destination destination = instance.createMessageType(GET_CUSTOMER_QUEUE);
        return instance.session.createProducer(destination);
    }

    private CustomerMessage createCustomerMessage() {
        return new CustomerMessage("VIET", 30, "NHA TRANG");
    }
}
