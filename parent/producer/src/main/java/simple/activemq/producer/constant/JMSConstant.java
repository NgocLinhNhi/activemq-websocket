package simple.activemq.producer.constant;

public class JMSConstant {

    // Name of the queue we will receive messages from
    public static final String GET_CUSTOMER_QUEUE = "GET_CUSTOMER_QUEUE";
}
