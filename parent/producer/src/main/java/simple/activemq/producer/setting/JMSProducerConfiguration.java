package simple.activemq.producer.setting;

import org.apache.activemq.ActiveMQConnectionFactory;
import simple.activemq.producer.config.LoadPropertiesFile;

import javax.jms.*;

public class JMSProducerConfiguration {
    private String brokerUrl;
    public Session session;

    private static JMSProducerConfiguration INSTANCE;

    public static JMSProducerConfiguration getInstance() throws Exception {
        if (INSTANCE == null) INSTANCE = new JMSProducerConfiguration();
        return INSTANCE;
    }

    private JMSProducerConfiguration() throws Exception {
        loadProperties();
        start();
    }

    private void start() throws JMSException {
        // Getting JMS connection from the server
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(brokerUrl);
        Connection connection = connectionFactory.createConnection();
        connection.start();
        createSession(connection);
    }

    private void createSession(Connection connection) throws JMSException {
        // Creating session for JMS messages
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
    }

    public Destination createMessageType(String messageType) throws JMSException {
        return session.createQueue(messageType);
    }

    private void loadProperties() throws Exception {
        brokerUrl = LoadPropertiesFile.getInstance().loadProperties().getProperty("broker_url");
    }
}
