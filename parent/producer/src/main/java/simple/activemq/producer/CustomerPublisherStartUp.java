package simple.activemq.producer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import simple.activemq.producer.producer.CustomerMessagePublisher;

public class CustomerPublisherStartUp {
    private static final Logger logger = LoggerFactory.getLogger(CustomerPublisherStartUp.class);

    public static void main(String[] args) {
        try {
            CustomerMessagePublisher.getInstance().createMessage();
        } catch (Exception e) {
            logger.error("Create Producer Message for Customer has Error {1}", e);
        }
    }
}
