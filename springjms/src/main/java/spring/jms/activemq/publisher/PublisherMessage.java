package spring.jms.activemq.publisher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
public class PublisherMessage {
    private static final Logger logger = LoggerFactory.getLogger(PublisherMessage.class);

    @Autowired
    private JmsTemplate jmsTemplate;

    public void sendMessage(String queueName, final String message) {
        logger.info("Message Sent: {} To queue name: {}", message, queueName);
        jmsTemplate.send(queueName, session -> session.createTextMessage(message));
    }

    public void sendMessage(String queueName, final Object message) {
        logger.info("Message Sent: {} To queue name: {}", message, queueName);
        jmsTemplate.send(queueName, session -> session.createObjectMessage((Serializable) message));
    }
}
