package spring.jms.activemq.obj_message;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class Student implements Serializable {

    private String name;
    private String address;
}
