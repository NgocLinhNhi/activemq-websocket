package spring.jms.activemq.subcriber;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;
import spring.jms.activemq.obj_message.Student;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;

@Component
public class SubcriberMessage {
    private static final Logger logger = LoggerFactory.getLogger(SubcriberMessage.class);

    //Consumer lắng nghe queue với name ex: NINH_LOVE.queue -> tạo từ localhost của activeMQ hoặc từ 1 publisher nào đó

    //Cách 1 : nhận message từ localhost / class test DemoApplicationTests
    // Xử lý xong message = NINH_LOVE.queue then send tới 1 queue/Topic mới là  VIET_LOVE.queue = annotation @SendTo
    @JmsListener(destination = "NINH_LOVE.queue")
    @SendTo("VIET_LOVE.queue")
    public String receiveTextMessage(Message jsonMessage) throws JMSException {
        String messageData;
        String response = null;

        //Send TextMessage
        if (jsonMessage instanceof TextMessage) {
            TextMessage textMessage = (TextMessage) jsonMessage;
            messageData = textMessage.getText();
            response = "Redirect Message " + messageData;

            logger.info("TextMessage : {}", textMessage.getText());
        }

        return response;
    }

    @JmsListener(destination = "OBJECT_MESSAGE.queue")
    public String receiveObjMessage(ObjectMessage jsonMessage) throws JMSException {
        String response = null;

        //send Object Message
        if (jsonMessage instanceof ObjectMessage) {
            Student objectMessage = (Student) jsonMessage.getObject();
            response = objectMessage.getName();

            logger.info("Object Message : {}", objectMessage.getName());
        }

        return response;
    }

}