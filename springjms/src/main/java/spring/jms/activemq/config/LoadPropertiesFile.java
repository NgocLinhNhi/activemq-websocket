package spring.jms.activemq.config;

import lombok.Getter;
import lombok.Setter;
import spring.jms.activemq.io.InputStreams;

import java.util.Properties;

@Getter
@Setter
class LoadPropertiesFile {

    private static LoadPropertiesFile INSTANCE;

    static LoadPropertiesFile getInstance() {
        if (INSTANCE == null) INSTANCE = new LoadPropertiesFile();
        return INSTANCE;
    }

    Properties loadProperties() throws Exception {
        Properties properties = new Properties();
        properties.load(InputStreams.getInputStream(getPropertiesFile()));
        return properties;
    }

    private String getPropertiesFile() {
        String file = System.getProperty("application.properties");
        if (file == null)
            file = "application.properties";
        return file;
    }

}
