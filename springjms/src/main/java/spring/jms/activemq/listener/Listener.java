package spring.jms.activemq.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import spring.jms.activemq.publisher.PublisherMessage;
import spring.jms.activemq.subcriber.SubcriberMessage;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

@Component
public class Listener {
    private static final Logger logger = LoggerFactory.getLogger(SubcriberMessage.class);

    @Autowired
    private PublisherMessage producer;

    //Cách 2 : nhận message từ localhost / từ class SubcriberMessage -> sendTo Đến Queue/topic "admin.queue" = method sendMessage
    // không phải dùng @SendTo("VIET_LOVE.queue")
    //consumer Lắng nghe customer.queue
    // và send đến admin queue = publisher
    @JmsListener(destination = "VIET_LOVE.queue")
    public void receiveMessage(Message jsonMessage) throws JMSException {
        String messageData = null;

        if (jsonMessage instanceof TextMessage) {
            TextMessage textMessage = (TextMessage) jsonMessage;
            messageData = textMessage.getText();
            logger.info("Nhận tin nhắn Cách 2 : {}", textMessage.getText());
        }

        producer.sendMessage("admin.queue", messageData);
    }
}