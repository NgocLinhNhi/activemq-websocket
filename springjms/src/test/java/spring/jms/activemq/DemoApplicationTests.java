package spring.jms.activemq;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import spring.jms.activemq.obj_message.Student;
import spring.jms.activemq.publisher.PublisherMessage;
import spring.jms.activemq.subcriber.SubcriberMessage;

@RunWith(SpringRunner.class)
@SpringBootTest
class DemoApplicationTests {

    @Test
    void contextLoads() {
    }

    @Autowired
    private PublisherMessage sender;


    @Autowired
    private SubcriberMessage receiver;

    //Debug or xem logger để thấy được luồng chạy của JMS ->SendTo
    //Từ đây gửi 1 message cho class SubcriberMessage hứng = queue NINH_LOVE.queue
    @Test
    public void sendMessageText() {
        sender.sendMessage("NINH_LOVE.queue", "I Love You");
        //sender.sendMessage("VIET_LOVE.queue", "I Love You");
    }

//    @Test
//    public void sendMessageObject() throws Exception {
//        sender.sendMessage("OBJECT_MESSAGE.queue", createStudent());
//    }

    public Student createStudent() {
        Student st = new Student();
        st.setName("VIET");
        st.setAddress("Nha Trang");
        return st;
    }
}
